package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> ordinationsDatoer = new ArrayList<>();

	public PN(LocalDate startDato, LocalDate slutDato, double antalEnheder) {
		super(startDato, slutDato);
		this.antalEnheder = antalEnheder;
	}
	// TODO

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public ArrayList<LocalDate> getOrdinationsDatoer() {
		return new ArrayList<>(ordinationsDatoer);
	}

	/**
	 * Registrer dagen givesDen, hvor der er givet en dosis af PN. Returner true,
	 * hvis givesDen er inden for ordinationens gyldighedsperiode. Returner false
	 * ellers, og datoen givesDen ignoreres.
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isBefore(super.getSlutDen()) && givesDen.isAfter(super.getStartDen())
				|| givesDen.isEqual(getStartDen()) || givesDen.isEqual(getSlutDen())) {
			ordinationsDatoer.add(givesDen);
			return true;
		}
		return false;
	}

	/**
	 * Returner antal gange ordinationen er anvendt.
	 */
	public int getAntalGangeGivet() {
		if (ordinationsDatoer.size() == 0) {
			return 0;
		}
		return ordinationsDatoer.size();
	}

	@Override
	public double samletDosis() {
		if (ordinationsDatoer.size() == 0) {
			return 0;
		}
		return ordinationsDatoer.size() * antalEnheder;
	}

	// (antal gange ordinationen er anvendt * antal enheder) / (antal dage mellem
	// første og sidste givning)
	@Override
	public double doegnDosis() {
		if (getOrdinationsDatoer().size() == 0) {
			return 0;
		} else {
			Collections.sort(getOrdinationsDatoer());
			int antalGyldigeDage = (int) ChronoUnit.DAYS.between(getOrdinationsDatoer().get(0),
					getOrdinationsDatoer().get(getOrdinationsDatoer().size() - 1)) + 1;
			double gnsDoegnDosis = samletDosis() / antalGyldigeDage;
			return gnsDoegnDosis;
		}
	}

	@Override
	public String getType() {
		return "PN";
	}

}
