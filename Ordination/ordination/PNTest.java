package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

public class PNTest {

	@Test
	// Test for nedregrænseværdi (LocalDate.of(2019, 1, 1)
	public void givDosisTest1() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertTrue(pn.givDosis(LocalDate.of(2019, 1, 1)));
		assertTrue(pn.getOrdinationsDatoer().contains(LocalDate.of(2019, 1, 1)));
	}

	@Test
	// Test for middelværdi (LocalDate.of(2019, 1, 3)
	public void givDosisTest2() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertTrue(pn.givDosis(LocalDate.of(2019, 1, 3)));
		assertTrue(pn.getOrdinationsDatoer().contains(LocalDate.of(2019, 1, 3)));
	}

	@Test
	// Test for øvre grænseværdi (LocalDate.of(2019, 1, 12)
	public void givDosisTest3() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertTrue(pn.givDosis(LocalDate.of(2019, 1, 12)));
		assertTrue(pn.getOrdinationsDatoer().contains(LocalDate.of(2019, 1, 12)));
	}

	@Test
	// Ugyldighedstest Test for nedregrænseværdi (LocalDate.of(2018, 12, 31)
	public void givDosisTest4() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertFalse((pn.givDosis(LocalDate.of(2018, 12, 31))));
		assertFalse(pn.getOrdinationsDatoer().contains(LocalDate.of(2018, 12, 31)));
	}

	@Test
	// Ugyldighedstest for øvregrænseværdi (LocalDate.of(2019, 1, 13)
	public void givDosisTest5() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertFalse((pn.givDosis(LocalDate.of(2019, 1, 13))));
		assertFalse(pn.getOrdinationsDatoer().contains(LocalDate.of(2019, 1, 13)));
	}

	// Test af getAntalGangeGivet()
	@Test
	public void getAntalGangeGivetTest1() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		pn.givDosis(LocalDate.of(2019, 1, 2));
		pn.givDosis(LocalDate.of(2019, 1, 3));
		pn.givDosis(LocalDate.of(2019, 1, 4));
		pn.givDosis(LocalDate.of(2019, 1, 5));

		assertEquals(4, pn.getAntalGangeGivet(), 0);
	}

	@Test
	public void getAntalGangeGivetTest2() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertEquals(0, pn.getAntalGangeGivet(), 0);
	}

	// Test af samletDosis
	@Test
	public void samletDosisTest1() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		pn.givDosis(LocalDate.of(2019, 1, 2));
		pn.givDosis(LocalDate.of(2019, 1, 3));
		pn.givDosis(LocalDate.of(2019, 1, 4));
		pn.givDosis(LocalDate.of(2019, 1, 5));

		assertEquals(48, pn.samletDosis(), 0);
	}

	@Test
	public void samletDosisTest2() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		pn.givDosis(LocalDate.of(2019, 1, 2));
		pn.givDosis(LocalDate.of(2019, 1, 3));
		pn.givDosis(LocalDate.of(2019, 1, 4));
		pn.givDosis(LocalDate.of(2019, 1, 5));
		pn.givDosis(LocalDate.of(2019, 1, 8));

		assertEquals(60, pn.samletDosis(), 0);
	}

	@Test
	public void samletDosisTest3() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertEquals(0, pn.samletDosis(), 0);
	}

	@Test
	public void doegnDosisTest1() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		pn.givDosis(LocalDate.of(2019, 1, 2));
		pn.givDosis(LocalDate.of(2019, 1, 3));
		pn.givDosis(LocalDate.of(2019, 1, 4));
		pn.givDosis(LocalDate.of(2019, 1, 5));

		assertEquals(12, pn.doegnDosis(), 0);
	}

	@Test
	public void doegnDosisTest2() {
		PN pn = new PN(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), 12);
		assertEquals(0, pn.doegnDosis(), 0);
	}
}
