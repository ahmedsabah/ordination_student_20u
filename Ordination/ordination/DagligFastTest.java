package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class DagligFastTest {

	@Test
	/*
	 * startDato: 2021,1,1 slutDato: 2021,1,5 4 doser dagligt, 2 stykker pr. dose
	 */
	public void samletDosisTest1() {
		DagligFast dagligfast1 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast1.opretDosis(LocalTime.of(8, 00), 2.00);
		dagligfast1.opretDosis(LocalTime.of(12, 00), 2.00);
		dagligfast1.opretDosis(LocalTime.of(18, 00), 2.00);
		dagligfast1.opretDosis(LocalTime.of(23, 00), 2.00);

		assertEquals(40.00, dagligfast1.samletDosis(), 0);
	}

	@Test
	/*
	 * startDato: 2021,1,1 slutDato: 2021,1,5 4 doser dagligt, 1 stk pr. dose
	 */
	public void samletDosisTest2() {
		DagligFast dagligfast2 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 1));
		dagligfast2.opretDosis(LocalTime.of(8, 00), 1.00);
		dagligfast2.opretDosis(LocalTime.of(12, 00), 1.00);
		dagligfast2.opretDosis(LocalTime.of(18, 00), 1.00);
		dagligfast2.opretDosis(LocalTime.of(23, 00), 1.00);

		assertEquals(4, dagligfast2.samletDosis(), 0);
	}

	@Test
	/*
	 * startDato: 2021,1,1 
	 * slutDato: 2021,1,5 
	 * 1 dose dagligt, 5 stk pr. dose
	 */
	public void samletDosisTest3() {
		DagligFast dagligfast3 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast3.opretDosis(LocalTime.of(12, 00), 5.00);

		assertEquals(25, dagligfast3.samletDosis(), 0.001);
	}

	@Test
	/*
	 * startDato: 2021,1,1
	 * slutDato: 2021,1,1
	 * 4 doser dagligt, 1 stk pr. dose
	 */
	public void samletDosisTest4() {
		DagligFast dagligfast4 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 1));
		dagligfast4.opretDosis(LocalTime.of(12, 00), 1.00);

		assertEquals(1, dagligfast4.samletDosis(), 0.001);
	}

	@Test
	/*
	 * startDato: 2021,1,1
	 * slutDato: 2021,1,5
	 * 4 doser dagligt, 0 stk pr. dose
	 */
	public void samletDosisTest5() {
		DagligFast dagligfast5 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast5.opretDosis(LocalTime.of(12, 00), 0.00);
		dagligfast5.opretDosis(LocalTime.of(12, 00), 0.00);
		dagligfast5.opretDosis(LocalTime.of(12, 00), 0.00);
		dagligfast5.opretDosis(LocalTime.of(12, 00), 0.00);

		assertEquals(0, dagligfast5.samletDosis(), 0.001);
	}

	// TestMetoder til doegnDosis() i Dagligfast-klassen

	@Test
	/*
	 * startDato: 2021,1,1
	 * slutDato: 2021,1,5
	 * 4 doser dagligt, 2 stk pr. dose
	 */
	public void doegnDosisTest1() {
		DagligFast dagligfast1 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast1.opretDosis(LocalTime.of(8, 00), 2.00);
		dagligfast1.opretDosis(LocalTime.of(12, 00), 2.00);
		dagligfast1.opretDosis(LocalTime.of(18, 00), 2.00);
		dagligfast1.opretDosis(LocalTime.of(23, 00), 2.00);

		assertEquals(8, dagligfast1.doegnDosis(), 0);
	}

	@Test
	/*
	 * startDato: 2021,1,1
	 * slutDato: 2021,1,1
	 * 4 doser dagligt, 1 stk pr. dose
	 */
	public void doegnDosisTest2() {
		DagligFast dagligfast2 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 1));
		dagligfast2.opretDosis(LocalTime.of(8, 00), 1.00);
		dagligfast2.opretDosis(LocalTime.of(12, 00), 1.00);
		dagligfast2.opretDosis(LocalTime.of(18, 00), 1.00);
		dagligfast2.opretDosis(LocalTime.of(23, 00), 1.00);

		assertEquals(4, dagligfast2.doegnDosis(), 0);
	}

	@Test
	/*
	 * startDato: 2021,1,1
	 * slutDato: 2021,1,5
	 * 1 dose dagligt, 5 stk pr. dose
	 */
	public void doegnDosisTest3() {
		DagligFast dagligfast3 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast3.opretDosis(LocalTime.of(12, 00), 5.00);
		assertEquals(5, dagligfast3.doegnDosis(), 0);
	}

	@Test
	/*
	 * startDato: 2021,1,1
	 * slutDato: 2021,1,1
	 * 1 dose dagligt, 3 stk pr. dose
	 */
	public void doegnDosisTest4() {

		DagligFast dagligfast4 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 1));
		dagligfast4.opretDosis(LocalTime.of(12, 00), 1.00);

		assertEquals(1, dagligfast4.doegnDosis(), 0);
	}

	@Test
	/*
	 * startDato: 2021,1,1
	 * slutDato: 2021,1,5
	 * 4 doser dagligt, 0 stk pr. dose
	 */
	public void doegnDosisTest5() {

		DagligFast dagligfast5 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast5.opretDosis(LocalTime.of(8, 00), 0.00);
		dagligfast5.opretDosis(LocalTime.of(12, 00), 0.00);
		dagligfast5.opretDosis(LocalTime.of(18, 00), 0.00);
		dagligfast5.opretDosis(LocalTime.of(23, 00), 0.00);

		assertEquals(0, dagligfast5.doegnDosis(), 0);
	}
	// Test til opretDosis() i Dagligfast-klassen

	@Test
	/*
	 * Tid = 08:00, Antal = 0
	 */
	public void opretDosisTest1() {
		DagligFast dagligfast1 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast1.opretDosis(LocalTime.of(8, 00), 0.00);
		Dosis[] doser = new Dosis[4];
		doser[0] = new Dosis(LocalTime.of(8, 00), 0.00);

		assertEquals(0, dagligfast1.getDoser()[0].getAntal(), 0.001);
		assertEquals(LocalTime.of(8, 00), dagligfast1.getDoser()[0].getTid());
	}

	@Test
	/*
	 * Tid = 08:00, Antal = 100
	 */
	public void opretDosisTest2() {
		DagligFast dagligfast2 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast2.opretDosis(LocalTime.of(8, 00), 100.00);
		Dosis[] doser2 = new Dosis[4];
		doser2[0] = new Dosis(LocalTime.of(8, 00), 100.00);

		assertEquals(100, dagligfast2.getDoser()[0].getAntal(), 0.001);
		assertEquals(LocalTime.of(8, 00), dagligfast2.getDoser()[0].getTid());
	}

	@Test
	/*
	 * Tid = 12:00, Antal = 0
	 */
	public void opretDosisTest3() {
		DagligFast dagligfast3 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast3.opretDosis(LocalTime.of(12, 00), 0.00);
		Dosis[] doser3 = new Dosis[4];
		doser3[1] = new Dosis(LocalTime.of(12, 00), 0.00);

		assertEquals(0, dagligfast3.getDoser()[1].getAntal(), 0.001);
		assertEquals(LocalTime.of(12, 00), dagligfast3.getDoser()[1].getTid());
	}

	@Test
	/*
	 * Tid = 12:00, Antal = 100
	 */
	public void opretDosisTest4() {
		DagligFast dagligfast4 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast4.opretDosis(LocalTime.of(12, 00), 100.00);
		Dosis[] doser4 = new Dosis[4];
		doser4[1] = new Dosis(LocalTime.of(12, 00), 100.00);

		assertEquals(100, dagligfast4.getDoser()[1].getAntal(), 0.001);
		assertEquals(LocalTime.of(12, 00), dagligfast4.getDoser()[1].getTid());
	}

	@Test
	/*
	 * Tid = 18:00, Antal = 0
	 */
	public void opretDosisTest5() {
		DagligFast dagligfast5 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast5.opretDosis(LocalTime.of(18, 00), 0.00);
		Dosis[] doser5 = new Dosis[4];
		doser5[2] = new Dosis(LocalTime.of(18, 00), 0.00);

		assertEquals(0, dagligfast5.getDoser()[2].getAntal(), 0.001);
		assertEquals(LocalTime.of(18, 00), dagligfast5.getDoser()[2].getTid());
	}

	@Test
	/*
	 * Tid = 18:00, Antal = 100
	 */
	public void opretDosisTest6() {
		DagligFast dagligfast6 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast6.opretDosis(LocalTime.of(18, 00), 100.00);
		Dosis[] doser6 = new Dosis[4];
		doser6[2] = new Dosis(LocalTime.of(18, 00), 100.00);

		assertEquals(100, dagligfast6.getDoser()[2].getAntal(), 0.001);
		assertEquals(LocalTime.of(18, 00), dagligfast6.getDoser()[2].getTid());
	}

	@Test
	/*
	 * Tid = 23:00, Antal = 0
	 */
	public void opretDosisTest7() {
		DagligFast dagligfast7 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast7.opretDosis(LocalTime.of(23, 00), 0.00);
		Dosis[] doser7 = new Dosis[4];
		doser7[1] = new Dosis(LocalTime.of(23, 00), 0.00);

		assertEquals(0, dagligfast7.getDoser()[3].getAntal(), 0.001);
		assertEquals(LocalTime.of(23, 00), dagligfast7.getDoser()[3].getTid());
	}

	@Test
	/*
	 * Tid = 23:00, Antal = 100
	 */
	public void opretDosisTest8() {
		DagligFast dagligfast8 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast8.opretDosis(LocalTime.of(23, 00), 100.00);
		Dosis[] doser8 = new Dosis[4];
		doser8[1] = new Dosis(LocalTime.of(23, 00), 100.00);

		assertEquals(100, dagligfast8.getDoser()[3].getAntal(), 0.001);
		assertEquals(LocalTime.of(23, 00), dagligfast8.getDoser()[3].getTid());
	}

	@Test
	/*
	 * Tid = 08:00, Antal = -1
	 */
	public void opretDosisTest9() {
		DagligFast dagligfast9 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast9.opretDosis(LocalTime.of(8, 00), -1.00);
		Dosis[] doser9 = new Dosis[4];
		doser9[0] = new Dosis(LocalTime.of(8, 00), -1.00);

		assertEquals(-1, dagligfast9.getDoser()[0].getAntal(), 0.001);
		assertEquals(LocalTime.of(8, 00), dagligfast9.getDoser()[0].getTid());
	}

	@Test
	/*
	 * Tid = 08:00, Antal = -100
	 */
	public void opretDosisTest10() {
		DagligFast dagligfast10 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast10.opretDosis(LocalTime.of(8, 00), -100.00);
		Dosis[] doser10 = new Dosis[4];
		doser10[0] = new Dosis(LocalTime.of(8, 00), -100.00);

		assertEquals(-100, dagligfast10.getDoser()[0].getAntal(), 0.001);
		assertEquals(LocalTime.of(8, 00), dagligfast10.getDoser()[0].getTid());
	}

	@Test
	/*
	 * Tid = 08:00, Antal = -100
	 */
	public void opretDosisTest11() {
		DagligFast dagligfast11 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast11.opretDosis(LocalTime.of(12, 00), -1.00);
		Dosis[] doser11 = new Dosis[4];
		doser11[1] = new Dosis(LocalTime.of(12, 00), -1.00);

		assertEquals(-1.00, dagligfast11.getDoser()[1].getAntal(), 0.001);
		assertEquals(LocalTime.of(12, 00), dagligfast11.getDoser()[1].getTid());
	}

	@Test
	/*
	 * Tid = 12:00, Antal = -100
	 */
	public void opretDosisTest12() {
		DagligFast dagligfast12 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast12.opretDosis(LocalTime.of(12, 00), -100.00);
		Dosis[] doser12 = new Dosis[4];
		doser12[1] = new Dosis(LocalTime.of(12, 00), -100.00);

		assertEquals(-100.00, dagligfast12.getDoser()[1].getAntal(), 0.001);
		assertEquals(LocalTime.of(12, 00), dagligfast12.getDoser()[1].getTid());
	}

	@Test
	/*
	 * Tid = 08:00, Antal = -100
	 */
	public void opretDosisTest13() {
		DagligFast dagligfast13 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast13.opretDosis(LocalTime.of(8, 00), 1.00);
		Dosis[] doser13 = new Dosis[4];
		doser13[0] = new Dosis(LocalTime.of(7, 59), 1.00);

		assertEquals(1.00, dagligfast13.getDoser()[0].getAntal(), 0.001);
		assertEquals(LocalTime.of(7, 59), dagligfast13.getDoser()[0].getTid());
	}

	@Test
	public void opretDosisTest14() {
		DagligFast dagligfast14 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast14.opretDosis(LocalTime.of(8, 00), -100.00);
		Dosis[] doser14 = new Dosis[4];
		doser14[0] = new Dosis(LocalTime.of(7, 59), -100.00);

		assertEquals(1.00, dagligfast14.getDoser()[0].getAntal(), 0.001);
		assertEquals(LocalTime.of(7, 59), dagligfast14.getDoser()[0].getTid());
	}

	@Test
	public void opretDosisTest15() {
		DagligFast dagligfast15 = new DagligFast(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 5));
		dagligfast15.opretDosis(LocalTime.of(8, 00), -100.00);
		Dosis[] doser15 = new Dosis[4];
		doser15[0] = new Dosis(LocalTime.of(7, 59), -100.00);

		assertEquals(1.00, dagligfast15.getDoser()[0].getAntal(), 0.001);
		assertEquals(LocalTime.of(7, 59), dagligfast15.getDoser()[0].getTid());
	}

}