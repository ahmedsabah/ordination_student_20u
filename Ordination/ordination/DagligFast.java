package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {

	private Dosis[] dagligFast = new Dosis[4];

	public DagligFast(LocalDate startDato, LocalDate slutDato) {
		super(startDato, slutDato);
	}

	public Dosis[] getDoser() {
		return dagligFast;
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		if (tid.equals(LocalTime.of(8, 0))) {
			dagligFast[0] = dosis;
		} else if (tid.equals(LocalTime.of(12, 0))) {
			dagligFast[1] = dosis;

		} else if (tid.equals(LocalTime.of(18, 0))) {
			dagligFast[2] = dosis;
		} else if (tid.equals(LocalTime.of(23, 0))) {
			dagligFast[3] = dosis;
		}
	}

	@Override
	public double samletDosis() {
		/*
		 * Retunere det totale antal af DagligFast pakken.
		 */
		double samledeDosis = 0;

		for (int i = 0; i < 4; i++) {
			if (dagligFast[i] != null) {
				samledeDosis += dagligFast[i].getAntal() * (ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1);
			}
		}

		return samledeDosis;

	}

	@Override
	public double doegnDosis() {
		/*
		 * Retunere den gennemsnitlige dosis givet pr. dag i den periode ordinationen er
		 * gyldig.
		 */
		int numberOfDays = (int) ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;

		double doegnDosis = samletDosis() / numberOfDays;

		return doegnDosis;

	}

	@Override
	public String getType() {
		/*
		 * Returnere den typen af ordination i tekst.
		 */
		return "Dette er en fast ordination";
	}

}
