package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import controller.Controller;

public class DagligSkaevTest {

	@SuppressWarnings("deprecation")
	@Test
	public void samletDosisTC1() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		DS.opretDosis(LocalTime.of(15, 0), 1);
		
		assertEquals(1.00, DS.samletDosis(), 0.001);
		
	}
	
	@Test
	public void samletDosisTC2() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 23));
		DS.opretDosis(LocalTime.of(8, 0), 2);
		
		assertEquals(2.00, DS.samletDosis(), 0.001);
		
	}
	
	@Test
	public void samletDosisTC3() {
		
		LocalTime[] klokkeSlet = {LocalTime.of(12, 0)};
		double[] antalEnheder = {1.00};
		
		try {
		DagligSkaev DS = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 24), LocalDate.of(2019, 1, 23), null,
				null, klokkeSlet, antalEnheder);
		fail();
	} catch (IllegalArgumentException e) {
		//assert
		assertEquals(e.getMessage(), "Ordinationen kan ikke gives, da slutdato er før startdato");
	}
		
	}
	
	@Test
	public void samletDosisTC4() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 23));
		DS.opretDosis(LocalTime.of(0, 0), 6);
		DS.opretDosis(LocalTime.of(6, 0), 6);
		DS.opretDosis(LocalTime.of(12, 0), 6);
		DS.opretDosis(LocalTime.of(18, 0), 6);
		DS.opretDosis(LocalTime.of(0, 0), 6);
		DS.opretDosis(LocalTime.of(6, 0), 6);
		DS.opretDosis(LocalTime.of(12, 0), 6);
		DS.opretDosis(LocalTime.of(18, 0), 6);
		
		assertEquals(48, DS.samletDosis(), 0.001);
		
	}
	
	@Test
	public void samletDosisTC5() {
		
		LocalTime[] klokkeSlet = {LocalTime.of(12, 0)};
		double[] antalEnheder = {-1.00};
		Patient patient = new Patient("Finn Madsen", "070985-1153", 83.2);
		Laegemiddel laegemiddel = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "styk");
		
		try {
		DagligSkaev DS = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24), patient,
				laegemiddel, klokkeSlet, antalEnheder);
		fail();
	} catch (IllegalArgumentException e) {
		//assert
		assertEquals(e.getMessage(), "Antal enheder skal være et positivt tal");
	}
		
	}
	
	@Test
	public void doegnDosisTC1() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		DS.opretDosis(LocalTime.of(0, 0), 1);
		DS.opretDosis(LocalTime.of(2, 0), 1);
		DS.opretDosis(LocalTime.of(4, 0), 1);
		DS.opretDosis(LocalTime.of(6, 0), 1);
		DS.opretDosis(LocalTime.of(8, 0), 1);
		DS.opretDosis(LocalTime.of(10, 0), 1);
		DS.opretDosis(LocalTime.of(12, 0), 1);
		DS.opretDosis(LocalTime.of(14, 0), 1);
		DS.opretDosis(LocalTime.of(16, 0), 1);
		DS.opretDosis(LocalTime.of(18, 0), 1);
		DS.opretDosis(LocalTime.of(20, 0), 1);
		DS.opretDosis(LocalTime.of(22, 0), 1);
		
		assertEquals(12, DS.doegnDosis(), 0.001);
		
	}
	
	@Test
	public void doegnDosisTC2() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 23));
		DS.opretDosis(LocalTime.of(0, 0), 2);
		DS.opretDosis(LocalTime.of(4, 0), 2);
		DS.opretDosis(LocalTime.of(8, 0), 2);
		DS.opretDosis(LocalTime.of(12, 0), 2);
		DS.opretDosis(LocalTime.of(16, 0), 2);
		DS.opretDosis(LocalTime.of(20, 0), 2);
		
		assertEquals(12, DS.doegnDosis(), 0.001);
	}
	
	@Test
	public void doegnDosisTC3() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		
		DS.opretDosis(LocalTime.of(6, 0), 6);
		DS.opretDosis(LocalTime.of(12, 0), 6);
		DS.opretDosis(LocalTime.of(18, 0), 6);
		DS.opretDosis(LocalTime.of(00, 0), 6);
		
		assertEquals(24, DS.doegnDosis(), 0.001);
		
	}
	
	
	@Test
	public void doegnDosisTC4() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		
		DS.opretDosis(LocalTime.of(8, 0), 4);
		DS.opretDosis(LocalTime.of(16, 0), 4);
		DS.opretDosis(LocalTime.of(00, 0), 4);
		
		assertEquals(12, DS.doegnDosis(), 0.001);
		
	}
	
	@Test
	public void opretDosisTC1() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		DS.opretDosis(LocalTime.of(0, 0), 2);
		
		assertEquals(2, DS.getFoersteDosisAntal(), 0.001);
		
	}
	
	@Test
	public void opretDosisTC2() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		DS.opretDosis(LocalTime.of(23, 59), 5);
		
		assertEquals(5, DS.getFoersteDosisAntal(), 0.001);
		
	}
	
	@Test
	public void opretDosisTC3() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		DS.opretDosis(LocalTime.of(12, 00), 6);
		
		assertEquals(6, DS.getFoersteDosisAntal(), 0.001);
		
	}
	
	@Test
	public void opretDosisTC4() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		DS.opretDosis(LocalTime.of(12, 30), 10);
		
		assertEquals(10, DS.getFoersteDosisAntal(), 0.001);
		
	}	
	
	@Test
	public void opretDosisTC5() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		DS.opretDosis(LocalTime.of(8, 30), 1);
		
		assertEquals(1, DS.getFoersteDosisAntal(), 0.001);
		
	}		
	
	@Test
	public void opretDosisTC6() {
		
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24));
		try {
		DS.opretDosis(LocalTime.of(15, 30), -2);
		fail();
	} catch (IllegalArgumentException e) {
		//assert
		assertEquals(e.getMessage(), "Antal skal være større eller lig med 0");
	}
		
	}
	
	
	
}
