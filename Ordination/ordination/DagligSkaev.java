package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination{
	
	private final ArrayList<Dosis> doser = new ArrayList<>();

    public DagligSkaev(LocalDate startDato, LocalDate slutDato) {
		super(startDato, slutDato);
		
	}
    
    public ArrayList<Dosis> getDoser(){
    	return new ArrayList<>(doser);
    }
    
    public double getFoersteDosisAntal() {
    	return doser.get(0).getAntal();
    }

	public void opretDosis(LocalTime tid, double antal) {
		if(antal >= 0) {
	        Dosis dosis = new Dosis(tid, antal);
	        doser.add(dosis);
		}
		else {
			throw new IllegalArgumentException("Antal skal være større eller lig med 0");
		}
    }
	
	public void removeDosis(Dosis dosis) {
		if(doser.contains(dosis)) {
			doser.remove(dosis);
		}
	}

	@Override
	public double samletDosis() {
		// Returner den totale dosis, der er givet i den periode ordinationen er gyldig.
		
		double antalFraDoser = 0;
		
		for(var d : doser) {
			antalFraDoser += d.getAntal();
		}
		
		return antalFraDoser;
		
		
	}

	@Override
	public double doegnDosis() {
		// Returner den gennemsnitlige dosis givet pr dag i den periode ordinationen er gyldig.
		
		int numberOfDays = 0;
		
		if(getStartDen() == getSlutDen()) {
			numberOfDays = 1;
		}
		else if(getSlutDen().isEqual(getStartDen().plusDays(1))) {
			numberOfDays = 1;
		}
		else {
			numberOfDays = 	(int) (ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) +1);
		}
		
		double samletDosis = samletDosis();
		
		return samletDosis/numberOfDays;
	}

	@Override
	public String getType() {
		// Returner ordinationstypen som en tekst
		
		return "Dette er en skæv ordination";
	}
}
