package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public abstract class Controller {
	private static Storage storage = Storage.getInstance();

	/**
	 * Opret en PN-ordination. Hvis startDato er efter slutDato kastes en
	 * IllegalArgumentException og ordinationen oprettes ikke.
	 */
	public static PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double antal) {
		if (checkStartFoerSlut(startDen, slutDen) == false) {
			throw new IllegalArgumentException("Ordination kan ikke gives, da slutdato er før startdato");
		} else if (patient == null) {
			throw new IllegalArgumentException("Patienten må ikke være null.");
		} else if (laegemiddel == null) {
			throw new IllegalArgumentException("Der skal være et gyldigt lægemiddel.");
		} else {
			PN pn = new PN(startDen, slutDen, antal);
			patient.addOrdination(pn);
			pn.setLaegemiddel(laegemiddel);
			return pn;
		}
	}

	/**
	 * Opret en DagligFast ordination. Hvis startDato er efter slutDato kastes en
	 * IllegalArgumentException og ordinationen oprettes ikke.
	 */
	public static DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		if (checkStartFoerSlut(startDen, slutDen) == false) {
			throw new IllegalArgumentException("Ordination kan ikke gives, da slutdato er før startdato");
		} else if (patient == null) {
			throw new IllegalArgumentException("Patienten må ikke være null.");
		} else if (laegemiddel == null) {
			throw new IllegalArgumentException("Der skal være et gyldigt lægemiddel.");
		} else {
			DagligFast dfOrd = new DagligFast(startDen, slutDen);
			if (morgenAntal >= 0) {
				dfOrd.opretDosis(LocalTime.of(8, 0), morgenAntal);
			}
			if (middagAntal >= 0) {
				dfOrd.opretDosis(LocalTime.of(12, 0), middagAntal);
			}
			if (aftenAntal >= 0) {
				dfOrd.opretDosis(LocalTime.of(18, 0), aftenAntal);
			}
			if (natAntal >= 0) {
				dfOrd.opretDosis(LocalTime.of(23, 0), natAntal);
			}
			patient.addOrdination(dfOrd);
			dfOrd.setLaegemiddel(laegemiddel);

			return dfOrd;
		}
	}

	/**
	 * Opret en DagligSkæv ordination. Hvis startDato er efter slutDato kastes en
	 * IllegalArgumentException og ordinationen oprettes ikke. Hvis antallet af
	 * elementer i klokkeSlet og antalEnheder er forskellige kastes en
	 * IllegalArgumentException.
	 */
	public static DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder) {
		if (checkStartFoerSlut(startDen, slutDen) == false) {
			throw new IllegalArgumentException("Ordination kan ikke gives, da slutdato er før startdato");
		} else if (patient == null) {
			throw new IllegalArgumentException("Patienten må ikke være null.");
		} else if (laegemiddel == null) {
			throw new IllegalArgumentException("Der skal være et gyldigt lægemiddel.");
		} else if (klokkeSlet.length != antalEnheder.length) {
			throw new IllegalArgumentException("Antallet af doser skal matche antallet af klokkeslæt");
		} else {
			DagligSkaev dsOrd = new DagligSkaev(startDen, slutDen);

			double dosis = 0;
			for (int i = 0; i < antalEnheder.length; i++) {
				if (dosis <= anbefaletDosisPrDoegn(patient, laegemiddel)) {
					if (antalEnheder[i] > 0) {
						dsOrd.opretDosis(klokkeSlet[i], antalEnheder[i]);
						dosis += antalEnheder[i];
					} else if (antalEnheder[i] < 0) {
						throw new IllegalArgumentException("Antal enheder skal være et positivt tal");
					}
				}
			}
			patient.addOrdination(dsOrd);
			dsOrd.setLaegemiddel(laegemiddel);
			return dsOrd;
		}
	}

	/**
	 * Tilføj en dato for anvendelse af PN ordinationen. Hvis datoen ikke er
	 * indenfor ordinationens gyldighedsperiode kastes en IllegalArgumentException.
	 */
	public static void ordinationPNAnvendt(PN ordination, LocalDate dato) {
		// TODO
		if (!ordination.givDosis(dato)) {
			throw new IllegalArgumentException("Datoen er ikke gyldig.");
		}
	}

	/**
	 * Returner den anbefalede dosis af lægemidlet for patienten. (Beregningen
	 * anvender en enhedsfaktor, der er afhængig af patientens vægt.)
	 */
	public static double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		// TODO
		double anbefaledeDosisPrDoegn = 0;
		if (patient.getVaegt() < 25) {
			anbefaledeDosisPrDoegn = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (patient.getVaegt() > 120) {
			anbefaledeDosisPrDoegn = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		} else {
			anbefaledeDosisPrDoegn = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return anbefaledeDosisPrDoegn;
	}

	/**
	 * Returner antal ordinationer af lægemidlet for patienter med vægt i
	 * intervallat vægtStart..vægtslut.
	 */
	public static int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut, Laegemiddel laegemiddel) {
		// TODO
		int ordinationer = 0;

		for (Patient p : Storage.getInstance().getAllPatienter()) {
			if (p.getVaegt() >= vægtStart && p.getVaegt() <= vægtSlut) {
				for (Ordination o : p.getOrdinationer()) {
					if (o.getLaegemiddel().equals(laegemiddel)) {
						ordinationer += 1;
					}
				}
			}
		}
		return ordinationer;

	}

	// -----------------------------------------------------

	/**
	 * Returner true, hvis slutDato <= slutDato.
	 */
	private static boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		return startDato.compareTo(slutDato) <= 0;
	}

	public static Patient opretPatient(String cpr, String navn, double vaegt) {
		Patient p = new Patient(cpr, navn, vaegt);
		storage.addPatient(p);
		return p;
	}

	public static Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
			double enhedPrKgPrDoegnTung, String enhed) {
		Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
				enhed);
		storage.addLaegemiddel(lm);
		return lm;
	}

	public static void initStorage() {
		opretPatient("121256-0512", "Jane Jensen", 63.4);
		opretPatient("070985-1153", "Finn Madsen", 83.2);
		opretPatient("050972-1233", "Hans Jørgensen", 89.4);
		opretPatient("011064-1522", "Ulla Nielsen", 59.9);
		opretPatient("090149-2529", "Ib Hansen", 87.7);

		opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
	}

	// -----------------------------------------------------

	public static List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public static List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}
}
