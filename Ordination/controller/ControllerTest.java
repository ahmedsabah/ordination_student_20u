package controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class ControllerTest {

	@Test // Test for PNOrdination m. patient = Jane, laegemiddel = null og antal = 0,
			// startDato < slutDato
	public void opretPNOrdinationTest1Exception() {
		Controller.initStorage();
		try {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
					Storage.getInstance().getAllPatienter().get(0), null, 0.0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Der skal være et gyldigt lægemiddel.");
		}
	}

	@Test // Test for PNOrdination m. patient = jane, laegemiddel = paracetamol og antal =
			// 0, startDato < slutDato
	public void opretPNOrdinationTest2() {
		Controller.initStorage();
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
				Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1), 0.0);
		assertTrue(Storage.getInstance().getAllPatienter().get(0).getOrdinationer().contains(pn));
	}

	@Test // Test for PNOrdination patient = Jane, m. laegemiddel = null og antal = 100,
			// startDato < slutDato
	public void opretPNOrdinationTest3() {
		Controller.initStorage();
		try {

			PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
					Storage.getInstance().getAllPatienter().get(0), null, 100.0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Der skal være et gyldigt lægemiddel.");
		}
	}

	@Test // Test for PNOrdination m. patient = Jane, laegemiddel = paracetamol og antal =
			// 100, startDato < slutDato
	public void opretPNOrdinationTest4() {
		Controller.initStorage();
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
				Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1),
				100.0);
		assertTrue(Storage.getInstance().getAllPatienter().get(0).getOrdinationer().contains(pn));
	}

	@Test // Test for PNOrdination m. patient = null, laegemiddel = null og antal = 0.0,
			// startDato < slutDato
	public void opretPNOrdinationTest5Exception() {
		Controller.initStorage();
		try {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12), null, null, 0.0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Patienten må ikke være null.");
		}
	}

	@Test // Test for PNOrdination m. patient = Jane, laegemiddel = paracetamol og antal =
			// 0.0,
			// startDato > slutDato
	public void opretPNOrdinationTest6Exception() {
		Controller.initStorage();
		try {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 13), LocalDate.of(2019, 1, 12),
					Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1),
					0.0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ordination kan ikke gives, da slutdato er før startdato");
		}
	}

	@Test // Test for PNOrdination m. patient = Jane, laegemiddel = paracetamol og antal =
			// 0.0,
			// startDato = slutDato
	public void opretPNOrdinationTest7() {
		Controller.initStorage();

		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 12), LocalDate.of(2019, 1, 12),
				Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1), 0.0);
		assertTrue(Storage.getInstance().getAllPatienter().get(0).getOrdinationer().contains(pn));
	}

	// *************************************************************************************************************************

	@Test // startDato > slutDato, patient = Finn, lægemiddel = paracetamol, morEnhed = 0,
	// midEnhed = 0, aftEnhed = 0, natEnhed = 0
	public void opretDagligFastOrdinationTest1() {
		Controller.initStorage();
		DagligFast dagligfast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
				LocalDate.of(2019, 1, 12), Storage.getInstance().getAllPatienter().get(1),
				Storage.getInstance().getAllLaegemidler().get(1), 0, 0, 0, 0);

		assertTrue(Storage.getInstance().getAllPatienter().get(1).getOrdinationer().contains(dagligfast));
	}

	@Test // startDato > slutDato, patient = Finn, lægemiddel = paracetamol, morEnhed =
			// 25,
	// midEnhed = 25, aftEnhed = 25, natEnhed = 25
	public void opretDagligFastOrdinationTest2() {
		Controller.initStorage();
		DagligFast dagligfast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
				LocalDate.of(2019, 1, 12), Storage.getInstance().getAllPatienter().get(1),
				Storage.getInstance().getAllLaegemidler().get(1), 25, 25, 25, 25);

		assertTrue(Storage.getInstance().getAllPatienter().get(1).getOrdinationer().contains(dagligfast));
	}

	@Test // startDato = slutDato, patient = Finn, lægemiddel = paracetamol, morEnhed =
	// 25, midEnhed = 25, aftEnhed = 25, natEnhed = 25
	public void opretDagligFastOrdinationTest3() {
		Controller.initStorage();
		DagligFast dagligfast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 12),
				LocalDate.of(2019, 1, 12), Storage.getInstance().getAllPatienter().get(1),
				Storage.getInstance().getAllLaegemidler().get(1), 25, 25, 25, 25);

		assertTrue(Storage.getInstance().getAllPatienter().get(1).getOrdinationer().contains(dagligfast));
	}

	@Test // startDato > slutDato, patient = Finn, lægemiddel = paracetamol, morEnhed = 2,
			// midEnhed = 2, aftEnhed = 2, natEnhed = 2
	public void opretDagligFastOrdinationException4() {
		Controller.initStorage();
		try {
			DagligFast dagligfast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 13),
					LocalDate.of(2019, 1, 12), Storage.getInstance().getAllPatienter().get(1),
					Storage.getInstance().getAllLaegemidler().get(1), 2, 2, 2, 2);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ordination kan ikke gives, da slutdato er før startdato");
		}
	}

	@Test // startDato < slutDato, patient = Finn, lægemiddel = null, morEnhed = 2,
			// midEnhed = 2, aftEnhed = 2, natEnhed = 2
	public void opretDagligFastOrdinationException5() {
		Controller.initStorage();
		try {
			DagligFast dagligfast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
					LocalDate.of(2019, 1, 12), Storage.getInstance().getAllPatienter().get(1), null, 2, 2, 2, 2);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Der skal være et gyldigt lægemiddel.");
		}
	}

	@Test // startDato < slutDato, patient = null, lægemiddel = paracetamol, morEnhed = 2,
			// midEnhed = 2, aftEnhed = 2, natEnhed = 2
	public void opretDagligFastOrdinationException6() {
		Controller.initStorage();
		try {
			DagligFast dagligfast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
					LocalDate.of(2019, 1, 12), null, Storage.getInstance().getAllLaegemidler().get(1), 2, 2, 2, 2);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Patienten må ikke være null.");
		}
	}

	// *************************************************************************************************************************
	@Test // startDato < slutDato, patient = Finn, lægemiddel = paracetamol, enheder[] ==
			// klokkeslæt
	public void opretDagligSkaevOrdinationTest1() {
		Controller.initStorage();
		LocalTime[] kl = { LocalTime.of(13, 0), LocalTime.of(17, 0), LocalTime.of(20, 00), LocalTime.of(23, 00) };
		double[] enheder = { 1.0, 1.0, 1.0, 1.0 };
		DagligSkaev ds = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
				Storage.getInstance().getAllPatienter().get(1), Storage.getInstance().getAllLaegemidler().get(1), kl,
				enheder);

		assertTrue(Storage.getInstance().getAllPatienter().get(1).getOrdinationer().contains(ds));
	}

	@Test // startDato == slutDato, patient = Finn, lægemiddel = paracetamol, enheder[] ==
			// klokkeslæt
	public void opretDagligSkaevOrdinationTest2() {
		Controller.initStorage();
		LocalTime[] kl = { LocalTime.of(13, 0), LocalTime.of(17, 0), LocalTime.of(20, 00), LocalTime.of(23, 00) };
		double[] enheder = { 1.0, 1.0, 1.0, 1.0 };
		DagligSkaev ds = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 24), LocalDate.of(2019, 1, 24),
				Storage.getInstance().getAllPatienter().get(1), Storage.getInstance().getAllLaegemidler().get(1), kl,
				enheder);

		assertTrue(Storage.getInstance().getAllPatienter().get(1).getOrdinationer().contains(ds));
	}

	@Test // startDato < slutDato, patient = Finn, lægemiddel = paracetamol, enheder[] !=
	// klokkeslæt
	public void opretDagligSkaevOrdinationTest3Exception() {
		Controller.initStorage();
		LocalTime[] kl = { LocalTime.of(13, 0), LocalTime.of(17, 0), LocalTime.of(20, 00), LocalTime.of(23, 00) };
		double[] enheder = { 1.0, 1.0, 1.0, 1.0, 3 };
		try {
			DagligSkaev ds = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
					Storage.getInstance().getAllPatienter().get(1), Storage.getInstance().getAllLaegemidler().get(1),
					kl, enheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Antallet af doser skal matche antallet af klokkeslæt");
		}
	}

	@Test // startDato < slutDato, patient = Finn, lægemiddel = paracetamol, enheder[] !=
	// klokkeslæt
	public void opretDagligSkaevOrdinationTest4Exception() {
		Controller.initStorage();

		double[] enheder = { 1.0, 1.0, 1.0, 1.0 };
		try {
			LocalTime[] kl = { LocalTime.of(28, 0), LocalTime.of(17, 0), LocalTime.of(20, 00), LocalTime.of(23, 00),
					LocalTime.of(23, 59) };
			DagligSkaev ds = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
					Storage.getInstance().getAllPatienter().get(1), Storage.getInstance().getAllLaegemidler().get(1),
					kl, enheder);
			fail();
		} catch (DateTimeException e) {
			assertEquals(e.getMessage(), "Invalid value for HourOfDay (valid values 0 - 23): 28");
		}
	}

	@Test // startDato < slutDato, patient = null, lægemiddel = paracetamol, enheder ==
			// klokkeslæt
	public void opretDagligSkaevOrdinationTest5Exception() {
		Controller.initStorage();
		double[] enheder = { 1.0, 1.0, 1.0, 1.0 };
		LocalTime[] kl = { LocalTime.of(13, 0), LocalTime.of(17, 0), LocalTime.of(20, 00), LocalTime.of(23, 00) };
		try {
			DagligSkaev ds = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
					null, Storage.getInstance().getAllLaegemidler().get(1), kl, enheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Patienten må ikke være null.");
		}
	}

	@Test // startDato < slutDato, patient = Finn, lægemiddel = null, enheder ==
			// klokkeslæt
	public void opretDagligSkaevOrdinationTest6Exception() {
		Controller.initStorage();
		double[] enheder = { 1.0, 1.0, 1.0, 1.0 };
		LocalTime[] kl = { LocalTime.of(13, 0), LocalTime.of(17, 0), LocalTime.of(20, 00), LocalTime.of(23, 00) };
		try {
			DagligSkaev ds = Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
					Storage.getInstance().getAllPatienter().get(1), null, kl, enheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Der skal være et gyldigt lægemiddel.");
		}
	}

	// *************************************************************************************************************************

	@Test // GivesDen = 2019/1/2
	public void ordinationPNAnvendtTest1() {
		Controller.initStorage();
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
				Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1), 2.0);
		pn.givDosis(LocalDate.of(2019, 1, 2));
		assertTrue(pn.getOrdinationsDatoer().contains(LocalDate.of(2019, 1, 2)));
	}

	@Test // GivesDen = 2019/1/1
	public void ordinationPNAnvendtTest2() {
		Controller.initStorage();
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
				Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1), 2.0);
		pn.givDosis(LocalDate.of(2019, 1, 1));
		assertTrue(pn.getOrdinationsDatoer().contains(LocalDate.of(2019, 1, 1)));
	}

	@Test // GivesDen = 2019/1/12
	public void ordinationPNAnvendtTest3() {
		Controller.initStorage();
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
				Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1), 2.0);
		pn.givDosis(LocalDate.of(2019, 1, 12));
		assertTrue(pn.getOrdinationsDatoer().contains(LocalDate.of(2019, 1, 12)));
	}

	@Test // GivesDen = 2018/1/1
	public void ordinationPNAnvendtTest4Exception() {
		Controller.initStorage();
		try {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
					Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1),
					2.0);
			pn.givDosis(LocalDate.of(2018, 1, 1));
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Datoen er ikke gyldig.");
		}
	}

	@Test // GivesDen = 2020/1/1
	public void ordinationPNAnvendtTest5Exception() {
		Controller.initStorage();
		try {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
					Storage.getInstance().getAllPatienter().get(0), Storage.getInstance().getAllLaegemidler().get(1),
					2.0);
			pn.givDosis(LocalDate.of(2020, 1, 1));
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Datoen er ikke gyldig.");
		}
	}

	// *************************************************************************************************************************

	@Test // Patient = Ulla Nielsen, 59,9 kg ; lægemiddel = Methotrexat, 0.01, 0.015, 0.02
	public void anbefaletDosisPrDoegnTest1() {
		Controller.initStorage();

		Controller.anbefaletDosisPrDoegn(Storage.getInstance().getAllPatienter().get(3),
				Storage.getInstance().getAllLaegemidler().get(3));

		assertEquals(0.8985, Controller.anbefaletDosisPrDoegn(Storage.getInstance().getAllPatienter().get(3),
				Storage.getInstance().getAllLaegemidler().get(3)), 0.01);
	}

	@Test // Patient = Dummy dummy, 20 ; lægemiddel = Methotrexat, 0.01, 0.015, 0.02
	public void anbefaletDosisPrDoegnTest2() {
		Controller.initStorage();

		Patient patient = Controller.opretPatient("00000000", "Dummy dummy", 20);

		Controller.anbefaletDosisPrDoegn(patient, Storage.getInstance().getAllLaegemidler().get(3));

		assertEquals(0.2, Controller.anbefaletDosisPrDoegn(patient, Storage.getInstance().getAllLaegemidler().get(3)),
				0.01);
	}

	@Test // Patient = Dummy dummy, 150 ; lægemiddel = Methotrexat, 0.01, 0.015, 0.02
	public void anbefaletDosisPrDoegnTest3() {

		Patient patient = Controller.opretPatient("00000000", "Dummy dummy", 150);

		Controller.anbefaletDosisPrDoegn(patient, Storage.getInstance().getAllLaegemidler().get(3));

		assertEquals(3.0, Controller.anbefaletDosisPrDoegn(patient, Storage.getInstance().getAllLaegemidler().get(3)),
				0.01);
	}

	// *************************************************************************************************************************
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest1() {
		
		Patient p1 = Controller.opretPatient("2525252525", "p1", 50);
		Patient p2 = Controller.opretPatient("2525252525", "p2", 70);
		Patient p3 = Controller.opretPatient("2525252525", "p3", 90);
		Patient p4 = Controller.opretPatient("2525252525", "p4", 150);
		
		Laegemiddel L2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		
		LocalTime[] klokkeSlet = {LocalTime.of(10, 0)};
		double[] aN = {1.00};
		
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p1, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p2, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p3, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p4, L2, klokkeSlet, aN);
		
		assertEquals(3, Controller.antalOrdinationerPrVægtPrLægemiddel(10, 100, L2));
		
		
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest2() {
		
		Patient p1 = Controller.opretPatient("2525252525", "p1", 50);
		Patient p2 = Controller.opretPatient("2525252525", "p2", 70);
		Patient p3 = Controller.opretPatient("2525252525", "p3", 90);
		Patient p4 = Controller.opretPatient("2525252525", "p4", 150);
		
		Laegemiddel L2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		
		LocalTime[] klokkeSlet = {LocalTime.of(10, 0)};
		double[] aN = {1.00};
		
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p1, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p2, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p3, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p4, L2, klokkeSlet, aN);
		
		assertEquals(1, Controller.antalOrdinationerPrVægtPrLægemiddel(130, 180, L2));
		
		
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest3() {
		
		Patient p1 = Controller.opretPatient("2525252525", "p1", 50);
		Patient p2 = Controller.opretPatient("2525252525", "p2", 70);
		Patient p3 = Controller.opretPatient("2525252525", "p3", 90);
		Patient p4 = Controller.opretPatient("2525252525", "p4", 150);
		
		Laegemiddel L2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		
		LocalTime[] klokkeSlet = {LocalTime.of(10, 0)};
		double[] aN = {1.00};
		
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p1, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p2, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p3, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p4, L2, klokkeSlet, aN);
		
		assertEquals(0, Controller.antalOrdinationerPrVægtPrLægemiddel(170, 210, L2));
		
		
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest4() {
		
		Patient p1 = Controller.opretPatient("2525252525", "p1", 50);
		Patient p2 = Controller.opretPatient("2525252525", "p2", 70);
		Patient p3 = Controller.opretPatient("2525252525", "p3", 90);
		Patient p4 = Controller.opretPatient("2525252525", "p4", 150);
		
		Laegemiddel L2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		
		LocalTime[] klokkeSlet = {LocalTime.of(10, 0)};
		double[] aN = {1.00};
		
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p1, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p2, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p3, L2, klokkeSlet, aN);
		Controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 10), p4, L2, klokkeSlet, aN);
		
		assertEquals(1, Controller.antalOrdinationerPrVægtPrLægemiddel(130, 180, L2));
		
		
	}

}
